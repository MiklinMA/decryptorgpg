#!/usr/bin/env bash

tar fxz app.tar.gz -C /usr/local/src
/usr/local/src/deploy.sh
/usr/local/src/run-tests.sh
