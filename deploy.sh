#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR
pwd

which httpd || yum install -y httpd
httpd -M | grep -q " wsgi_module" || yum install -y mod_wsgi
which pip || yum install -y python-pip
pip -q install -r requirements.txt

httpd_conf="/etc/httpd/conf.d/decryptMessage.conf"
cat httpd.conf | sed "s#%DIR%#$DIR#" > $httpd_conf && echo 'Config updated'

cat wsgi.py | sed "s#%DIR%#$DIR#" > decryptor.wsgi && echo 'WSGI updated'

mkdir -p .gnupg
chown -R apache:apache .gnupg
chmod 755 .gnupg

systemctl restart httpd && echo 'Service restarted'
