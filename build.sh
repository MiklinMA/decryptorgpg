#!/usr/bin/env bash

rm app.tar.gz

tar fcz app.tar.gz \
    deploy.sh \
    run-tests.sh \
    *.py \
    requirements.txt \
    httpd.conf \
    tests
