import os
import unittest
import json, urllib2

from flask_app import app, gpg

host = 'http://localhost:5000'

tests = []
def load_files():
    tests = []
    for fname in os.listdir('tests'):
        with open('tests/' + fname, 'r') as f:
            secret = os.path.splitext(fname)
            if secret[1] != '.asc':
                continue
            secret = secret[0]
            tests.append((secret, f.read()))
    return tests

def make_test_files(secret, message):
    def test(self):
        result = gpg.decrypt(message, passphrase=secret)
        self.assertTrue(result.ok)
    return test

def make_test_functions(secret, message):
    def test(self):
        with app.test_client() as c:
            result = c.post('/decryptMessage', json={
                'Passphrase': secret,
                'Message': message,
                })
            self.assertTrue(result.json)
            self.assertEqual(result.json.get('Error', 'success'), 'success')
    return test

def make_test_requests(secret, message):
    def test(self):
        data = json.dumps({
            'Passphrase': secret,
            'Message': message,
            })
        req = urllib2.Request(host + '/decryptMessage', data,
                {'Content-Type': 'application/json'})
        result = urllib2.urlopen(req)
        jdata = result.read()
        result.close()

        jdata = json.loads(jdata)
        self.assertTrue(jdata)
        self.assertEqual(jdata.get('Error', 'success'), 'success')
    return test

class Test1_Simple(unittest.TestCase):
    pass

class Test2_Flask(unittest.TestCase):
    pass

class Test3_Request(unittest.TestCase):
    def test_1_get(self):
        try:
            urllib2.urlopen(host + '/decryptMessage')
            self.assertTrue(False, 'GET')
        except urllib2.HTTPError as e:
            self.assertEqual(e.code, 405)

    def test_2_no_data(self):
        data = json.dumps({})
        req = urllib2.Request(host + '/decryptMessage', data,
                {'Content-Type': 'application/json'})
        result = urllib2.urlopen(req)
        jdata = result.read()
        result.close()

        jdata = json.loads(jdata)
        self.assertTrue(jdata)
        self.assertEqual(jdata.get('Error'), 'wrong request data')

    def test_3_wrong_data(self):
        data = json.dumps({
            'Secret': 'secret',
            'Message': 'message',
            })
        req = urllib2.Request(host + '/decryptMessage', data,
                {'Content-Type': 'application/json'})
        result = urllib2.urlopen(req)
        jdata = result.read()
        result.close()

        jdata = json.loads(jdata)
        self.assertTrue(jdata)
        self.assertEqual(jdata.get('Error'), 'Passphrase not found in request data')

    def test_4_wrong_data_2(self):
        data = json.dumps({
            'Passphrase': 'secret',
            'Text': 'message',
            })
        req = urllib2.Request(host + '/decryptMessage', data,
                {'Content-Type': 'application/json'})
        result = urllib2.urlopen(req)
        jdata = result.read()
        result.close()

        jdata = json.loads(jdata)
        self.assertTrue(jdata)
        self.assertEqual(jdata.get('Error'), 'Message not found in request data')

    def test_5_wrong_passphrase(self):
        data = json.dumps({
            'Passphrase': tests[0][0][1:],
            'Message': tests[0][1],
            })
        req = urllib2.Request(host + '/decryptMessage', data,
                {'Content-Type': 'application/json'})
        result = urllib2.urlopen(req)
        jdata = result.read()
        result.close()

        jdata = json.loads(jdata)
        self.assertTrue(jdata)
        self.assertEqual(jdata.get('Error'), 'GPG: decryption failed')

if __name__ == '__main__':
    ip = urllib2.urlopen('http://ipinfo.io/ip')
    host = 'http://%s' % ip.read().strip()
    tests = load_files()
    for k, (secret, message) in enumerate(tests, 1):
        setattr(Test1_Simple, 'test_%d' % k,
                make_test_files(secret, message))
        setattr(Test2_Flask, 'test_%d' % k,
                make_test_functions(secret, message))
        setattr(Test3_Request, 'test_9%d_decrypt_%s' % (k, secret),
                make_test_requests(secret, message))


    unittest.main(verbosity=2)

