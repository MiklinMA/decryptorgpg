import os
from flask import Flask, jsonify, request, abort
import gnupg

app = Flask(__name__)
gpg = gnupg.GPG(gnupghome=os.path.dirname(os.path.realpath(__file__))+'/.gnupg')

@app.route('/decryptMessage', methods=['POST'])
def decryptMessage():
    if not request.json:
        return jsonify({ 'Error': 'wrong request data' })

    message = request.json.get('Message')
    if not message:
        return jsonify({ 'Error': 'Message not found in request data' })

    secret = request.json.get('Passphrase')
    if not secret:
        return jsonify({ 'Error': 'Passphrase not found in request data' })

    result = gpg.decrypt(message, passphrase=secret)
    if not result.ok:
        return jsonify({ 'Error': "GPG: " + result.status })

    return jsonify({
        'DecryptedMessage': str(result)
    })

if __name__ == '__main__':
    app.run(debug=True)

