# Decrtyptor GPG

A simple apache web service that uses GPG to decrypt a message.

## TODO

- [x] Do not assume any specific starting directory when deploy.sh is executed.
- [x] The web service accepts a JSON payload with the parameters 'passphrase' and 'message'.
- [x] The web service can be executed using an HTTP POST.
- [x] The web service returns a JSON response that is either an error message
(if bad input parameters are given)
- [x] or a single response parameter (if good input parameters are given):
DecryptedMessage - The given Message, decrypted using GPG and the given Passphrase.
- [x] The web service has unit tests that you believe are sufficient
(/usr/local/src/run-tests.sh)
- [x] After running, details about the tests are output (pass, fail, specific failure messages)

